import React, { useEffect } from "react";
import { Row, Spin } from "antd";
import queryString from "query-string";
import { useLocation, useHistory } from "react-router-dom";
import { setCookie } from "./utils";

const LoginView = ({ match }) => {
  let location = useLocation();
  const history = useHistory();
  const params = queryString.parse(location.search);
  // slack oauth example
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/install/auth?code=${params.code}`)
      .then(res => res.json())
      .then(res => {
        setCookie("token", res.token);
        history.push("/");
      });
  }, [history, params.code]);
  return (
    <Row
      type="flex"
      style={{ minHeight: 200, width: "100%" }}
      align="middle"
      justify="center"
    >
      <Spin size="large" tip="Logging in..."></Spin>
    </Row>
  );
};

export default LoginView;
