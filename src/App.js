import React from 'react';
import './App.css';
import { Button } from 'antd';

import { getCookie } from './utils';
import Login from './Login';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import { ApolloProvider } from "@apollo/react-hooks";
import ApolloClient from "apollo-client";
import { ApolloLink } from "apollo-link";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

const middlewareLink = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: { authorization: `Bearer ${getCookie("token")}` }
  });
  return forward(operation);
});

const cache = new InMemoryCache();
const link = new HttpLink({
  uri: process.env.REACT_APP_APOLLO_CLIENT_URL
});

const client = new ApolloClient({
  cache,
  link: ApolloLink.from([middlewareLink, link])
});

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/login">
        <Login />
      </Route>
      <Route path="*">
        <Main />
      </Route>
    </Switch>
  </Router>
);

const Main = () => (
  <div>
    Ready. <Button type="primary">Primary</Button>
    <Button type="danger">Danger</Button>
    <Button>Default</Button>
  </div>
);

export default () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
);
