export const setCookie = (
  name,
  value,
  expires = false,
  path = "/",
  domain = null,
  secure = false
) => {
  const c = `${name}=${escape(value)}${expires ? `; expires=${expires}` : ""}${
    path ? `; path=${path}` : ""
  }${domain ? `; domain=${domain}` : ""}${secure ? "; secure" : ""}`;
  document.cookie = c;
};

// Get cookie.
export const getCookie = name => {
  const cookie = ` ${document.cookie}`;
  const search = ` ${name}=`;
  let setStr = null;
  let offset = 0;
  let end = 0;
  if (cookie.length > 0) {
    offset = cookie.indexOf(search);
    if (offset !== -1) {
      offset += search.length;
      end = cookie.indexOf(";", offset);
      if (end === -1) {
        end = cookie.length;
      }
      setStr = unescape(cookie.substring(offset, end));
    }
  }
  return setStr;
};
